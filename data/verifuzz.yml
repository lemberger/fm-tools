name: VeriFuzz
input_languages:
  - C
project_url: https://www.tcs.com
spdx_license_identifier: LicenseRef-TCSEvaluationLicense
benchexec_toolinfo_module: "https://gitlab.com/sosy-lab/software/benchexec/-/raw/main/benchexec/tools/verifuzz.py"
fmtools_format_version: "2.0"
fmtools_entry_maintainers:
  - m.raveendra.kumar

maintainers:
  - name: Raveendra Kumar Medicherla
    institution: Tata Consultancy Services
    country: India
    url: null

versions:
  - version: "svcomp24"
    doi: null
    benchexec_toolinfo_options: []
    required_ubuntu_packages:
      - clang-12
      - gcc-multilib
      - libc6-dev-i386
      - llvm-12
      - openjdk-8-jdk-headless
      - python2
      - python3-sklearn
      - python3-pandas
  - version: "svcomp23"
    url: "https://gitlab.com/sosy-lab/sv-comp/archives-2023/-/raw/main/2023/verifuzz.zip"
    benchexec_toolinfo_options: []
    required_ubuntu_packages:
      - clang-12
      - gcc-multilib
      - libc6-dev-i386
      - llvm-12
      - openjdk-8-jdk-headless
      - python2
      - python3-sklearn
      - python3-pandas

competition_participations:
  - competition: "SV-COMP 2023"
    track: "Verification"
    tool_version: "svcomp23"
    jury_member:
      name: Raveendra Kumar Medicherla
      institution: Tata Consultancy Services
      country: India
      url: null

techniques:
  - Bounded Model Checking
  - Numeric Interval Analysis
  - Evolutionary Algorithms
  - Explicit-Value Analysis
  - Floating-Point Arithmetics
  - Guidance by Coverage Measures
  - Random Execution

licenses:
  - name: TCS Evaluation License
    body: |
      EVALUATION AGREEMENT
      This Evaluation Agreement ("Agreement") is a legal agreement between you 
      ("You", "Your" or "Licensee") and Tata Consultancy Services Limited ("Licensor" 
      Or "TCS"), and governs your possession and/or use of the TCS' VeriFuzz software 
      and documentation ("Software"). By clicking and/or checking an "I Agree" or any 
      similar button or check box presented with these terms at the time of use, or 
      by accessing, downloading, installing, and/or by activating the Software with 
      any associated license key, or using, as applicable, all or any part of the 
      Software, you agree to the terms of this Agreement, and agree that You are 
      responsible for compliance with any applicable local laws.
      If You are accessing, downloading, installing or using the Software on behalf 
      of an organization/entity You represent, You are agreeing to these terms and 
      conditions on behalf of Your organization/entity, and You represent and warrant 
      that You have legal authority to bind your organization/entity to the terms of 
      this Agreement. In that case, "You", "Your" or "Licensee" also refers to that 
      organization/entity. 
      If You do not agree with any of these terms, you must not download, install, 
      activate, use or access the Software in any way, or type "I accept" or check 
      and/or click "I Agree" or any similar box or button associated with this 
      Agreement during the software downloading, installation, activation, use or 
      access process. You must promptly destroy the Software and/or access 
      key/credentials provided to You.

      1. Evaluation Terms. 
      a) Subject to the terms and conditions set forth in this Agreement, and in 
      consideration of the Licensee's acceptance thereof, TCS grants to Licensee a 
      non-exclusive, personal, non-transferable, non-sub licensable, non-commercial, 
      right to use the Software in object code form, within premises and computer 
      systems in Your possession and control, only for the limited purpose of 
      Licensee's academic research evaluation in a test environment to evaluate 
      SV-COMP benchmarks of 12th Competition on Software Verification (SV-COMP 2023) 
      held at TACAS 2023 and publish the results of evaluation in accordance with the 
      competition rules and not for production or other productive use in any manner 
      whatsoever, in accordance with instructions and guidelines provided in the 
      accompanying documentation, or as may be specified by TCS from time to time. 
      All fixes, patches, upgrades and updates for the Software that TCS may (in its 
      sole discretion) provide shall be deemed to be the part of and treated as 
      Software licensed hereunder. 
      
      b) In particular, and without limitation, the Licensee represents, 
      warrants and covenants, that it shall NOT and shall not permit others to:
      - use, copy or modify, enhance or create derivative works of the Software 
      or any portion thereof, except as expressly permitted in this Agreement, except 
      that the TACAS Competition Chair hosting the SV-COMP 2023 competition may 
      retain a copy of the Software for the purposes of the competition and for 
      future reference in later versions of the SV-COMP competition; 
      - sell, license, sublicense, assign, transfer, distribute or timeshare 
      the Software or otherwise grant any right under this Agreement to any third 
      party, without the prior written consent of TCS;
      - reverse engineer, disassemble, de-compile, tamper, recreate, enhance or 
      modify the Software or any part thereof;
      - remove, alter, obscure or otherwise render illegible any of TCS's logo, 
      trademark, copyright notice or other proprietary or confidentiality markings 
      that may be placed on the Software or any component or output thereof;
      - override access authorization and access controls for the Licensee's 
      access and use to the Software as may be prescribed by TCS or circumvent, 
      bypass, delete or remove any form of protection, or usage, functionality or 
      technical restrictions or limitations, or to enable functionality disabled by 
      TCS, in connection with the Software
      - use the Software to store or transmit malicious code, files, scripts, 
      agents or programs intended to do harm, including, for example, viruses, worms, 
      time bombs and Trojan horses;
      - use the Software in any manner or for any purpose that violates any 
      applicable law or the rights of others;
      - interfere with or disrupt the integrity or performance of any Software 
      or data contained therein;
      - perform or disclose any security testing, including without limitation, 
      penetration testing, remote access testing, network discovery, vulnerability 
      scanning, password cracking, etc., of the Software and/or the service or 
      associated infrastructure without TCS' prior written consent;
      - interface or link or include, without express written permission, the 
      Software with any other systems or applications or services other than those 
      agreed in writing and in accordance with the documentation provided by TCS; 
      - use any public software or open source software in connection with 
      Software in any manner that requires, pursuant to the license applicable to 
      such public or open source software, that the Software be disclosed or 
      distributed in source code form, or made available free or charge to 
      recipients, or modifiable without any restriction by recipients; or
      - access or use the Software for purposes of the development, provision 
      or use of a competing software, service or product except as permitted herein 
      or any other purpose that is to the TCS's detriment or commercial disadvantage.
      c) Licensee shall be responsible for all activity occurring under its 
      control and ensure that they abide by all applicable local, state, national and 
      foreign laws, treaties and regulations in connection with their use of the 
      Software, including those related to data privacy, international communications 
      and the transmission of technical or personal data. Licensee shall not export 
      or re-export Software or technical data (or direct products thereof) provided 
      under this Agreement in violation of any applicable export control laws and 
      regulations.
      d) Licensee agrees that TCS may, not more than once every six months and 
      upon not less than ten (10) days notice to Licensee, audit Licensee's use of 
      the Software for compliance with the terms & conditions of this Agreement. If 
      any audit reveals Licensee to be in breach of this Agreement, TCS shall be 
      entitled to terminate the license granted hereunder, without prejudice to any 
      other rights or remedies TCS may have under this Agreement or otherwise.
      e) As between TCS and Licensee, Licensee shall be responsible for (a) 
      acquisition, installation and maintenance of the Target Environment; (b) 
      complying with the applicable terms and conditions of the respective third 
      party products used in Target Environment (c) choice, installation, use and 
      maintenance of any third party software not forming part of the Software, which 
      is and/or to be used in relation to the Software, (d) making regular back-ups 
      of Licensee's data processed via the Software, and (e) complying with any laws 
      or regulations applicable to Licensee's industry that are generally not 
      applicable to licensed product providers. Target Environment as defined herein 
      shall mean to included Licensee's processing computer system hardware, software 
      and operating environment required for the operation and use of the Software as 
      described in the relevant documentation.
      f) Licensee may, upon written advance notice to TCS, relocate the Software 
      to another permitted site ("Permitted Site") provided that the Software shall 
      not be made available at more than one Permitted Site at any given time, and as 
      soon as Licensee relocates the Software to a new Permitted Site, Licensee will 
      notify TCS of the address of the new Permitted Site in writing. Copies which 
      are no longer needed must immediately be destroyed.
      
      2.Ownership and Proprietary Rights.
      a) Licensee acknowledges and agrees that TCS and / or its licensors does 
      and will continue to own all intellectual property and intellectual property 
      rights in or attached to the Software, including without limitation, in or 
      attached to any Derivative Works (as hereinafter defined) of the Software, 
      whether solely or jointly conceived, or even if made by, on behalf of, or for 
      the Licensee.  Nothing contained herein shall be construed as a transfer, 
      assignment or conveyance by TCS to Licensee of the ownership, interest or title 
      to the intellectual property or intellectual property rights in or attached to 
      the Software or any Derivative Works thereof. The Licensee only receives the 
      right to use as granted hereunder until terminated in accordance with this 
      Agreement. Derivative Works as defined herein shall mean to include works that 
      are based upon or derived from the Software or other proprietary material, 
      including without limitation, a revision, modification, customization, 
      enhancement, improvement, additions, interfaces, adapters, translation, 
      abridgment, condensation, expansion or any other form in which such material or 
      any proprietary portion thereof may be recast, transformed, or adapted. 
      b) Licensee acknowledges and agrees that TCS shall have the right to 
      collect, use, disclose, publish, or otherwise exploit without restriction or 
      compensation to Licensee, including without limitation, in future releases or 
      further developments, any technical data relating to the use of the Software, 
      comments, contributions or feedback that Licensee provides regarding the 
      Software and reference the license granted to the Licensee; or any and all 
      knowledge and information arising out of use of the Software (including 
      Derivative Works, if any).
      
      3.Third Party Software
      a) Notwithstanding the Use grant in Section 1, Licensee acknowledges that 
      certain components of the Software may be covered by so-called "open source" 
      software licenses ("Open Source Software" or "OSS"). Open Source Software, if 
      any, contained in this Software and the use thereof, shall be subject to the 
      terms and conditions of the relevant open source license agreement only. Open 
      Source Software and related information (including relevant notices) for the 
      currently licensed version of the Software are mentioned in the accompanying 
      notices and information documentation, namely, Third Party Notices. To the 
      extent the terms of the licenses applicable to OSS prohibit any of the 
      restrictions in this Agreement with respect to such OSS, such restrictions will 
      not apply to such OSS. To the extent the terms of the licenses applicable to 
      OSS require licensor to make an offer to provide source code or related 
      information in connection with the Software, such offer is hereby made, and may 
      be requested in writing by the Licensee. 
      b) The Software may contain third party proprietary software components. 
      Notwithstanding the Use grant in Section 1, Third party software, if any, 
      contained in this Software and the use thereof, shall be subject to the terms 
      and conditions of the respective third party vendor/licensor. Warranties, if 
      any, shall be from the respective third party vendor/licensor as specified in 
      their license agreement, with the limitations/restrictions as applicable. The 
      license agreement/s in respect of the embedded third party software, if any, 
      is/are provided in the notices and information documentation, namely ‘Third 
      Party Notices’.
      
      4.Warranty Disclaimer and Limitation of Liability. The Software 
      (including any Derivative Works) and related information, including without 
      limitation, Confidential Information and any third party software component (if 
      any) contained in the Software is/are provided "AS IS" without any warranty of 
      any kind, whether expressed or implied, including, but not limited to, any 
      implied warranty of merchantability, non-infringement, fitness for a particular 
      purpose, system integration, accuracy, reliability, error-free, 
      un-interruption, support (unless otherwise specified), correction, repair or 
      otherwise. In no event shall TCS or any of its licensor or any third party be 
      liable for any direct, indirect, incidental, special, exemplary or 
      consequential damages suffered by Licensee or any third party (including but 
      not limited to, use or loss of use; loss or damage of data, lost profits, 
      business, revenue, goodwill or anticipated savings; business interruption; 
      procurement of substitute goods or services) however caused and on any theory 
      of liability, whether in contract, strict liability, or tort (including 
      negligence or otherwise) arising from or as a result of using the  Software, 
      even if advised of the possibility of such damage.
      
      5.Termination
      a) If You breach any provisions of this Agreement or violate any law, rule 
      or regulation, Your access and use of the Software is automatically terminated 
      and any subsequent access and use is unauthorized.
      b) Either Party may terminate this Agreement for any reason immediately 
      upon notice with no obligation or liability of any kind on the part of the 
      other Party except as to which has already accrued or otherwise agreed to under 
      this Agreement. In the event of termination or expiration of this Agreement, 
      Licensee shall cease to use the Software, in whatever form disclosed hereunder, 
      immediately upon expiration or termination of the Agreement. All copies of 
      Software received by Licensee shall be deleted from Licensee's computer systems 
      in the event of termination or expiration of this Agreement and on request from 
      TCS, Licensee shall certify in writing its compliance with this paragraph to 
      TCS within five (5) days of termination or expiration of this Agreement. The 
      provisions of Sections 1 (b),(c),(d), 2, 3, 6(a),(c),(d) to (f) shall survive 
      expiration or termination of this Agreement.
      
      6.Miscellaneous:
      a) Indemnification: Licensee agrees to indemnify and hold TCS and its 
      subsidiaries, affiliates, officers, agents, employees, co-branders or other 
      partners, and licensors harmless from any claim or demand or expense, including 
      reasonable attorneys' fees, due to or arising out of Licensee's use of the 
      Software in a manner contrary to the provisions of this Agreement and 
      documentation or any matters connected therewith.
      b) No Assignment: Licensee shall not sell, transfer or assign any right or 
      obligation hereunder, except as expressly provided herein, without the prior 
      written consent of TCS.  Any assignment in violation of this sub-clause shall 
      be void.
      c) Waiver: Either Party's failure to exercise any right under this 
      Agreement shall not constitute a waiver of any other terms or conditions of 
      this Agreement with respect to any other or subsequent breach, or a waiver by 
      such Party of its right at any time thereafter to require exact and strict 
      compliance with the terms of this Agreement. In order to be effective, all 
      waivers under this Agreement must be in writing and signed by the waiving Party.
      d) Governing Law & Dispute Resolution: This Agreement shall be governed by 
      laws of State of New York, USA, without giving effect to the conflicts of law 
      principles thereof. Subject to the arbitration provisions below, courts in the 
      city of New York shall have the exclusive jurisdiction over any matter arising 
      out of or connected with this Agreement. Each Party hereby irrevocably waives, 
      to the fullest extent permitted by the law, any right to a trial by jury in any 
      legal proceeding arising out of or related to this Agreement or the 
      transactions contemplated hereby.
      Nothing herein shall prohibit TCS from seeking a temporary restraining order, 
      preliminary injunction or other provisional relief if, in its judgment, such 
      action is necessary to avoid irreparable damage, to preserve the status quo, or 
      to prevent or stop the violation of these terms and conditions and/or 
      infringement of intellectual property rights or other proprietary rights or 
      Confidential Information of TCS, its affiliates and/or their respective 
      licensors, or from bringing and pursuing legal action to specifically enforce 
      the provisions of this clause. 
      e) Notices: All notices provided for or permitted under this Agreement to 
      TCS shall be deemed effective upon receipt, and shall be in writing and (i) 
      delivered personally, (ii) sent by commercial overnight courier with written 
      verification of receipt, (iii) sent via electronic mail to 
      verifuzz.tool@tcs.com or (iv) sent by certified or registered mail, postage 
      prepaid and return receipt requested. Notices to TCS shall be sent to the 
      attention of its Legal Department, Tata Consultancy Services Limited, 101 Park 
      Avenue, NY 10178, NY, with a copy shall be sent to the General Counsel, Tata 
      Consultancy Services Limited, TCS House, Raveline Street, Fort, Mumbai 400001, 
      India. Notices to Licensee shall be sent at the email address provided by 
      Licensee at the time of requesting for Use of the Software.
      f) Severability: If any provision of this Agreement be held invalid or 
      unenforceable by a competent court, such provision shall be modified to the 
      extent necessary to make it valid and enforceable whilst preserving the intent 
      of the Parties and all other provisions of this Agreement shall remain fully 
      valid and enforceable unless otherwise agreed between the Parties.
      g) Entire Agreement: This Agreement along with all documents referenced 
      therein shall constitute the complete agreement between the Parties and 
      supersede all prior or contemporaneous agreements or representations, written 
      or oral, concerning the subject matter of this Agreement. This Agreement may 
      not be modified or amended except in writing signed by a duly authorized 
      representative of each Party. By using the Software and/or Service, Licensee 
      acknowledges that Licensee has read this Agreement, understands it, and agrees 
      to be bound by the terms and conditions.

      Copyright © 2022 Tata Consultancy Services Limited. All Rights Reserved.